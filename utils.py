from matplotlib import pyplot as pp
import matplotlib.lines as mlines
import numpy as np
import keras
import pconv_layer


def plot_scatter(x,y,diag=False):
    fig, ax = pp.subplots()
    pp.scatter(x,y)
    if diag:
        line = mlines.Line2D([0, 10], [0, 10], color='red')
        ax.add_line(line)
    pp.show()



def plot_as_image(matrix):
    pp.imshow(matrix)
    pp.show()


def get_nn(tile,mask,k,channels=[0]):
    nx,ny,_ = tile.shape
    liste_forages = list()
    for i in range(nx):
        for j in range(ny):
            if mask[i,j,0] == 1:
                liste_forages += [np.hstack(
                    ((i-nx//2)**2 + (j-ny//2)**2,tile[i,j,channels])
                    )] # le débit est le premier channel
    liste_forages_tri = sorted(liste_forages,key=lambda x:x[0])
    return liste_forages_tri[0:k]


def predict_nn(tile,mask,k,sigma=1): # prédit avec noyau gaussien
    Q_pred = float()
    normalization = float()
    nn = get_nn(tile,mask,k)
    for neighbor in nn:
        dist,Q = neighbor
        w = np.exp(-dist/(2*sigma**2))
        Q_pred += w*Q
        normalization += w
    if not nn:
        return np.nan
    return Q_pred / normalization


def correlation(x,y):
    correct_preds = ~np.isnan(x) & ~np.isnan(y)
    return np.corrcoef(x[correct_preds],y[correct_preds])[0,1]

def mse(x,y):
    correct_preds = ~np.isnan(x) & ~np.isnan(y)
    return float(keras.losses.MeanSquaredError()(x[correct_preds],y[correct_preds]))
    
    
    
    
#### ANN ####
def get_partial_convolutional_model_complex(shape, selected_channels):
        input_data = keras.Input(shape=shape)
        input_mask = keras.Input(shape=shape)
        c1,m1 = pconv_layer.PConv2D(10, (5,5), strides=2,activation='relu',kernel_initializer=keras.initializers.Ones())([input_data,input_mask])
        c2,m2 = pconv_layer.PConv2D(3, (5,5), strides=2,activation='relu',kernel_initializer=keras.initializers.Ones())([c1,m1])
        c3,m3 = pconv_layer.PConv2D(3, (5,5), strides=2,activation='relu',kernel_initializer=keras.initializers.Ones())([c2,m2])
        c4,m4 = pconv_layer.PConv2D(3, (5,5), strides=2,activation='relu',kernel_initializer=keras.initializers.Ones())([c3,m3])
        c5,m5 = pconv_layer.PConv2D(3, (5,5), strides=2,activation='relu',kernel_initializer=keras.initializers.Ones())([c4,m4])
        c6,m6 = pconv_layer.PConv2D(1, (5,5), strides=2,activation='relu',kernel_initializer=keras.initializers.Ones())([c5,m5])
        pool = keras.layers.Flatten()(c6)
        out = pool
        model = keras.Model([input_data,input_mask], out)
        return model
        
        
def get_partial_convolutional_model_small(shape, selected_channels):
        input_data = keras.Input(shape=shape)
        input_mask = keras.Input(shape=shape)
        c1,m1 = pconv_layer.PConv2D(1, (41,41), strides=1,activation='linear',
                                    trainable=False,
                                    kernel_initializer=keras.initializers.Ones())([input_data[:,:,:,0:1],input_mask[:,:,:,0:1]])
        features_layer = keras.layers.Concatenate()([input_data[:,19,19,:],c1[:,19,19]])
        flat_features = keras.layers.Flatten()(features_layer)
        hiden_layer = keras.layers.Dense(5,activation='relu')(flat_features)
        out = keras.layers.Dense(1,activation='relu')(hiden_layer)
        model = keras.Model([input_data,input_mask], out)
        return model



